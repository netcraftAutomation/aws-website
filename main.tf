terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  

  # S3 backend statically coded as variables are not supported in backend
  backend "s3" {
    bucket     = "tf-remote-state-replica20211010202017909900000001"
    key        = "aws-wbst/terraform.tfstate"
    region     = "eu-west-2"
    encrypt    = true
    kms_key_id = "8ac4e133-748b-4852-8f40-596e5d8fc52c"
  }
}


# Variables passed in as enviroment variables must first be declared

variable "AWS_ACCESS_KEY_ID" {
  type = string
}

variable "AWS_SECRET_ACCESS_KEY" {
  type = string
}

provider "aws" {
  region = "eu-west-2"
}


# Locals do not change between runs. Since we currently only have one 
# environment, locals is preferred over variables. 
locals {
  name = "NetcraftAutomation3"
}

resource "aws_s3_bucket" "website_bucket" {
  bucket = lower(local.name)

  # Private ACL since Cloudfront will be used as CDN of website.
  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }
}