## Name
AWS Website Repository - Terraform - Gitlab CI

## Description
This projects aims to:

* Provide several templates for Terraform CICD pipelines with Gitlab CI. These will be branched off main as stable releases.
* Demonstrate the principals of TERF(Terraform Enterprise Ready Framework) in practice
* Create a React SPA using AWS S3, Cloudfront and Serverless architecture for business logic
* Showcase my personal coding ability

## Roadmap

### Released:
* Basic Terraform CICD Pipeline for a single main branch

### WIP:
* Terraform modules for a cloudfront, S3 website

### Future Works:
* CICD pipeline working with multiple staged folders and multiple long running branches
  
## Contributing
This project is open for contribution and feedback. 
  
## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
  
## License
MIT licence. 
    
  
## Project status
Active Development.

